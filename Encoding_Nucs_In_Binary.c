// compile with gcc Trans_Nucs_To_Bits.c -O0 -Wall -o Trans_Nucs_To_Bits

#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

#define list_size 1000000

void reset_file();
void get_nucs(char x[list_size]);
void print_nucs_as_bits(char *nucleotide_list);
void print_bin(int x);
char nuc2bits(char nuc);
char get_bits(char bits_list[list_size]);
void set_2bits_in_byte(char *bytes, size_t len, unsigned byte_pos,
		       unsigned pos_in_byte, unsigned bit0, unsigned bit1);
void ask_continue(char x[]);
void bits2nucs(char y[]);
void file(char x[], char y[]);

int main()
{
  char nucleotide_list[list_size];
  reset_file();
  get_nucs(nucleotide_list);
  print_nucs_as_bits(nucleotide_list);          
  int length = 2 * strlen(nucleotide_list);
  char bits_list[length];
  get_bits(bits_list);
  ask_continue(bits_list);
}

//makes 2 bit sequences possible
void set_2bits_in_byte(char *bytes, size_t len, unsigned byte_pos,
		       unsigned pos_in_byte, unsigned bit0, unsigned bit1)
{
  assert(pos_in_byte < 4);
  assert(bit0 == 0 || bit0 == 1);
  assert(bit1 == 0 || bit1 == 1);
  char new_byte_val = 0;
  new_byte_val = ((bit1 << 1) | bit0) << (2 * pos_in_byte);
  bytes[byte_pos] = new_byte_val | bytes[byte_pos];
}

//resets sequence_in_bits.txt
void reset_file()
{
  FILE * fpointer;
  fpointer = fopen("sequence_in_bits.txt", "w");
  fprintf(fpointer, "");
  fclose(fpointer);
}


//opens input file and puts it into an array
void file(char x[], char y[])
{
  //x is place2
  strcat(y, x);
  FILE * fpoi;
  fpoi = fopen(y, "r");
  char c;
  int i;
  int n = 0;
  for (i = 0; i < strlen(x); i++)
    {
      x[i] = ' ';
    }
  if ((c = fgetc(fpoi)) == '>')
    {
      for (i = 0; (c = fgetc(fpoi)) != EOF && n == 0; i++)
	{
	  if (c == '\n')
	    {
	      n++;
	      x[0] = c;
	      break;
	    }
	}
    }
  else
    {
      i = 0;
      x[i] = c;
    }
  for (i = 1; (c = fgetc(fpoi)) != EOF; i++)
    {
      x[i] = c;
    }
  fclose(fpoi);
}

//gets nucleotides from file input if it is a file
void get_nucs(char x[list_size])
{
  //change from mateo to whatever your name on your computer is
  char place1[] = "/home/mateo/Downloads/";

  int i;
  int z = 0;
  char y[list_size];
  printf("What File Is Your Sequence In: \n(FASTA Files Are Required)\n");
  fgets(y, list_size, stdin);
  int v = strlen(y);
  y[v - 1] = '\0';
  for (i = 0; i < (strlen(y)); i++)
    {
      x[i] = y[i];
      if (x[i] == '.' && y[++i] == 'f')
	{
	  i--;
	  z = 1;
	}
    }
  if (z == 1)
    {
      file(x, place1);
    }
  int num_nucleotides = strlen(x);
  x[num_nucleotides - 1] = '\0';
  int num_bits = 2 * num_nucleotides;
  int num_bytes = num_bits * sizeof(char);
  char *bytes_ = malloc(num_bytes);
}

//turns nucleotides into binary
void print_nucs_as_bits(char *nucleotide_list)
{
  int i;
  for (i = 0; i < strlen(nucleotide_list); ++i)
    {
      char nuc = nucleotide_list[i];
      print_bin(nuc2bits(nuc));
    }
  printf("\n");
  int n_nucleotides = strlen(nucleotide_list);
  int n_bits = 2 * n_nucleotides;
  int n_bytes = n_bits * sizeof(char);
  char *bytes = malloc(n_bytes);
}

//function that translates nucleotides into binary
void print_bin(int x)
{
  FILE * fpointer;
  fpointer = fopen("sequence_in_bits.txt", "a");
  long i;
  int y = 0;
  for (i = 2; i > 0; i = i / 2)
    {
      if(x == '4')
	{
	  if (y == 0)
	    {
	      putchar ('1');
	      putchar ('0');
	      putchar ('0');
	      fprintf(fpointer, " 100");
      	      y++;
	    }
	  else if (y == 1)
	    {
	      y--;
	    }
	}
      else if (x == '5')
	{
	  if (y == 0)
	    {
	      putchar ('1');
	      putchar ('0');
	      putchar ('1');
	      fprintf(fpointer, " 101");
	      y++;
	    }
	  else if (y == 1)
	    {
	      y--;
	    }
	}
      else if (x == '6')
	{
	  if (y == 0)
	    {
	      putchar ('1');
	      putchar ('1');
	      putchar ('0');
	      fprintf(fpointer, " 110");
	      y++;
	    }
	  else if (y == 1)
	    {
	      y--;
	    }
	}
      else if (x == '7')
	{
	  if (y == 0)
	    {
	      putchar ('1');
	      putchar ('1');
	      putchar ('1');
	      fprintf(fpointer, " 111");
	      y++;
	    }
	  else if (y == 1)
	    {
	      y--;
	    }
	}
      else if (x == '8')
	{
	  if (y == 0)
	    {
	      putchar ('0');
	      putchar ('0');
	      putchar ('0');
	      fprintf(fpointer, " 000");
	      y++;
	    }
	  else if (y == 1)
	    {
	      y--;
	    }
	}
      else if (x == '9')
	{
	  if (y == 0)
	    {
	      putchar ('1');
	      putchar ('0');
	      putchar ('0');
	      putchar ('1');
	      fprintf(fpointer, "\t1001");
	      y++;
	    }
	  else if (y == 1)
	    {
	      y--;
	    }
	}
      else if (x == 'a')
	{
	  if (y == 0)
	    {
	      putchar ('1');
	      putchar ('0');
	      putchar ('1');
	      putchar ('0');
	      fprintf(fpointer, "\t1010");
	      y++;
	    }
	  else if (y == 1)
	    {
	      y--;
	    }
	}
      else if (x == 'b')
	{
	  if (y == 0)
	    {
	      putchar ('1');
	      putchar ('0');
	      putchar ('1');
	      putchar ('1');
	      fprintf(fpointer, "\t1011");
	      y++;
	    }
	  else if (y == 1)
	    {
	      y--;
	    }
	}
      else if (x == 'c')
	{
	  if (y == 0)
	    {
	      putchar ('1');
	      putchar ('1');
	      putchar ('0');
	      putchar ('0');
	      fprintf(fpointer, "\t1100");
	      y++;
	    }
	  else if (y == 1)
	    {
	      y--;
	    }
	}
      else if (x == 'd')
	{
	  if (y == 0)
	    {
	      putchar ('1');
	      putchar ('1');
	      putchar ('0');
	      putchar ('1');
	      fprintf(fpointer, "\t1101");
	      y++;
	    }
	  else if (y == 1)
	    {
	      y--;
	    }
	}
      else if (x == 'e')
	{
	  if (y == 0)
	    {
	      putchar ('1');
	      putchar ('1');
	      putchar ('1');
	      putchar ('0');
	      fprintf(fpointer, "\t1110");
	      y++;
	    }
	  else if (y == 1)
	    {
	      y--;
	    }
	}
      else if((x & i) != 0)
	{
	  putchar ('1');
	  fprintf(fpointer, "1");
	}
      else
	{
	  putchar ('0');
	  fprintf(fpointer, "0");
	}
    }
  fprintf(fpointer, "\n");
  fclose(fpointer);
}

//assigns translatable values to nucleotides
char nuc2bits(char nuc)
{
  if (nuc == 'A')
    {
      return '0';
    }
  else if (nuc == 'G')
    {
      return '1';
    }
  else if (nuc == 'C')
    {
      return '2';
    }
  else if (nuc == 'T')
    {
      return '3';
    }
  else if (nuc == 'R')
    {
      return '5';
    }
  else if (nuc == 'Y')
    {
      return '6';
    }
  else if(nuc == 'S')
    {
      return '7';
    }
  else if (nuc == 'W')
    {
      return '8';
    }
  else if (nuc == 'K')
    {
      return '9';
    }
  else if (nuc == 'M')
    {
      return 'a';
    }
  else if (nuc == 'B')
    {
      return 'b';
    }
  else if (nuc == 'D')
    {
      return 'c';
    }
  else if (nuc == 'H')
    {
      return 'd';
    }
  else if (nuc == 'V')
    {
      return 'e';
    }
  else
    {
      nuc = 'N';
      return '4';
    }
}

//takes binary out of its file
char get_bits(char bits_list[list_size])
{
  int bit = 0;
  int i = 0;
  int size;
  FILE * fpointer;
  fpointer = fopen("sequence_in_bits.txt", "r");
  while (bit != EOF)
    {
      bits_list[i] = (bit = fgetc(fpointer));
      i++;
    }
  fclose(fpointer);
  size = strlen(bits_list);
  bits_list[size - 1] = '\0';
}

//asks the user if they want to translate back into nucleotides
void ask_continue(char x[])
{
  //x is bit list
  printf("Would you like to translate back? y/n\n" );
  char answer = getchar();
  if (answer == 'Y' || answer == 'y')
    {
      bits2nucs(x);
    }
  else
    {
      printf("Okay, your data has been stored in sequence_in_bits.txt\n");
    }
}

//translates binary into nucleotides
void bits2nucs(char y[])
{
  //y is bit list
  int i = 0;
  while (i != (strlen(y)) + 2)
    {
      if (y[i] == '0')
	{
	  i++;
	  if (y[i] == '0')
	    {
	      printf("A");
	    }
	  else if (y[i] == '1')
	    {
	      printf("G");
	    }
	  else
	    {
	      printf("ERROR: %c", y[i]);
	    }
	}
      else if (y[i] == '1')
	{
	  i++;
	  if (y[i] == '0')
	    {
	      printf("C");
	    }
	  else if (y[i] == '1')
	    {
	      printf("T");
	    }
	  else
	    {
	      printf(" ERROR: %c", y[i]);
	    }
	}
      else if (y[i] == ' ')
	{
	  i++;
	  if (y[i] == '1')
	    {
	      i++;
	      if (y[i] == '0')
		{
		  i++;
		  if (y[i] == '0')
		    {
		      printf("N");
		    }
		  else if (y[i] == '1')
		    {
		      printf("R");
		    }
		}
	      else if (y[i] == '1')
		{
		  i++;
		  if (y[i] == '0')
		    {
		      printf("Y");
		    }
		  else if (y[i] == '1')
		    {
		      printf("S");
		    }
		}
	    }
	  else if (y[i] == '0')
	    {
	      i++;
	      if (y[i] == '0')
		{
		  i++;
		  if (y[i] == '0')
		    {
		      printf("W");
		    }
		}
	    }
	}
      else if (y[i] == '\t')
	{
	  i++;
	  if (y[i] == '1')
	    {
	      i++;
	      if (y[i] == '0')
		{
		  i++;
		  if (y[i] == '0')
		    {
		      i++;
		      if (y[i] == '1')
			{
			  printf("K");
			}
		    }
		  else if (y[i] == '1')
		    {
		      i++;
		      if (y[i] == '0')
			{
			  printf("M");
			}
		      else if (y[i] == '1')
			{
			  printf("B");
			}
		    }
		}
	      else if (y[i] == '1')
		{
		  i++;
		  if (y[i] == '0')
		    {
		      i++;
		      if (y[i] == '0')
			{
			  printf("D");
			}
		      else if (y[i] == '1')
			{
			  printf("H");
			}
		    }
		  else if (y[i] == '1')
		    {
		      i++;
		      if (y[i] == '0')
			{
			  printf("V");
			}
		    }
		}
	    }
	}
      i++;
    }
  printf("\n");
}
